const controller = {};
const { Mongoose } = require("mongoose");
const tiposEquipo =  require("./../models/tiposEquipo.model");
const EquiposModel = require("./../models/equipos.model");

controller.listar = (req, res) => {
  const filtro = req.query.filtro;

  var condition = filtro ? {
    $or: [
      { nombre: { $regex: `.*${filtro}*.`, $options: 'i' } },
      { referencia: { $regex: `.*${filtro}*.`, $options: 'i' } },
    ]
  } : null;

  var findId = EquiposModel;
  if (condition != null) {
    findId = EquiposModel.where(condition);
  }

  findId.find(function (err, equipos) {
    if (!equipos || equipos.length === 0) {
      return res.status(200).send({
        status: false,
        message: "No hay equipos",
      });
    }
    return res.status(200).send({
      status: true,
      message: "Se encontraron " + equipos.length + " equipos",
      equipos: equipos,
    });
  });
};

controller.listarPorId = (req, res) => {
  const { id } = req.params;

  const findId = EquiposModel.where({ _id: id });
  try {
    findId.findOne(function (err, equipo) {
      if (!equipo) {

        return res.status(404).send({
          status: false,
          message: `The device with id ${id} not exist`,
        });
      } else {

        // tiposEquipo.find(function (err, equipos) {
        //   console.log(err, equipos);
        // });

        // Tipos_equipo.find({}, function (err, docs) {
        //   // console.log(err, docs);
        // });

        // Tipos_equipo.findById(equipo.tipo_id, function (errTipEquipo, tipEquipo) {
        //   console.log(errTipEquipo, tipEquipo);
        //   // if (!tipEquipo) {
        //   //   equipo.tipo = {
        //   //     estado: false
        //   //   };
        //   // } else {
        //   //   equipo.tipo = tipEquipo;
        //   // }

          return res.status(200).send({
            status: true,
            message: "se encontro un dispositivo",
            equipos: equipo || "",
          });
        // });
      }
    });
  } catch (error) {
    return new Error(error);
  }
};

module.exports = controller;
