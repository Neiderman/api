/* eslint-disable max-len */
const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const bcrypt = require("bcrypt");

const Usuarios = new Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  fecha_ultimo_ingreso: {
    type: Date,
    default: Date.now,
  },
  rol_id: {
    type: Schema.Types.ObjectId
  },
  persona_id: {
    type: Schema.Types.ObjectId
  }
});

Usuarios.methods.comparePassword = function (candidatePassword, cb) {
  bcrypt.compare(candidatePassword.toString(), this.password, (err, isMatch) => {
    cb(err, isMatch);
  });
};

module.exports = mongoose.model("Usuarios", Usuarios);
